//
//  NewsModel.swift
//  TestNewsApp
//
//  Created by Yurii on 2/17/16.
//  Copyright © 2016 Nostris. All rights reserved.
//

import UIKit
import CFNetwork
import Foundation
import OHHTTPStubs

let kUrlForNews : String! = "https://yalantis.com/blog/"
let kCacheFile : String! = NSTemporaryDirectory() + "/cacheData"
class NewsItem: NSObject {
    
    var title:String?
    var url:String?
    var image:String?
    var author:String?
    var text:String?
    var views:Int?
    init(title:String?, url:String?, image:String?, author:String?, text:String?, views:Int?) {
        self.title = title
        self.url = url
        self.author = author
        self.image = image
        self.views = views
        self.text = text
    }
}
class NewsModel: NSObject {
    var cacheData:NSDictionary?
    
    override init() {
//        print( NSURL(string: kUrlForNews)?.path)
        cacheData = NSDictionary(contentsOfFile: kCacheFile)
        stub(isHost("yalantis.com") && isPath("/blog")) { _ in
            let data : NSData = NSData(contentsOfFile: NSBundle.mainBundle().pathForResource("test", ofType: "json")!)!
            return OHHTTPStubsResponse(data:data, statusCode:200, headers:nil).requestTime(1.0, responseTime: 3.0)
        }
    }
    
    static let sharedInstance:NewsModel = NewsModel()
    
    private var news : [AnyObject] = []
    
    private func parse(data:NSDictionary!) -> [NewsItem] {
        let items : [AnyObject] = data["data"] as! [AnyObject]
        var result : [NewsItem] = []
        for item in items {
            result.append(NewsItem(title:item["title"] as? String, url:item["url"] as? String, image:item["image"] as? String, author:item["author"] as? String, text:item["description"] as? String, views:item["viewsCount"] as? Int))
        }
        return result
    }
    
    func getNews(useCache useCache:Bool, callback:(Bool, [NewsItem])->Void) {
        if (useCache) {
            if (cacheData != nil) {
                callback(true, self.parse(cacheData))
            } else {
                callback(false, [])
            }
        }
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: kUrlForNews)!) { (data, response, error) -> Void in
            if (error != nil) {
                callback(false, [])
            } else if (data != nil){
                var json:NSDictionary?
                do {
                    json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary
                    if (json!.objectForKey("data") != nil) {
                        json!.writeToFile(kCacheFile, atomically: true)
                        self.cacheData = json
                    } else {
                        json = self.cacheData
                    }
                    callback(true, self.parse(json))
                }
                catch {
                    json = nil
                    callback(false, [])
                }
                
            }
        }.resume()
    }
}
