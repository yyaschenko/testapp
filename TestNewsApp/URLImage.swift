//
//  URLImage.swift
//  TestNewsApp
//
//  Created by Yurii on 2/17/16.
//  Copyright © 2016 Nostris. All rights reserved.
//

import UIKit

class URLImage: UIImage {
    static var cacheMap : NSMutableDictionary = NSMutableDictionary()
    func downloadImage(url:String!, callback:(NSURL?, NSError?) -> Void) {
        let fileCache : NSURL? = URLImage.cacheMap.objectForKey(url) as? NSURL
        if (fileCache != nil && NSFileManager.defaultManager().fileExistsAtPath(fileCache!.absoluteString)) {
            callback(URLImage.cacheMap.objectForKey(url) as? NSURL, nil)
            return
        }
        NSURLSession.sharedSession().downloadTaskWithURL(NSURL(string: url)!) { (file:NSURL?, response:NSURLResponse?, error:NSError?) -> Void in
            if (error != nil) {
                callback(nil, error)
                return
            }
            if (file != nil) {
                callback(file, nil)
                URLImage.cacheMap.setObject(file!, forKey: url)
            }
            }.resume()

    }
    
}
