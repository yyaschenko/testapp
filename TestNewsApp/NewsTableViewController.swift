//
//  NewsTableViewController.swift
//  TestNewsApp
//
//  Created by Yurii on 2/17/16.
//  Copyright © 2016 Nostris. All rights reserved.
//

import UIKit

class NewsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView : UITableView!
    var loadCache = true
    var isLoading = false
    
    var data : [NewsItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshControl)
        self.refresh(nil)
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("NewCell", forIndexPath: indexPath)
        (cell.viewWithTag(1) as! UILabel!).text = self.data[indexPath.row].title
        (cell.viewWithTag(2) as! UILabel!).text = self.data[indexPath.row].text
        (cell.viewWithTag(3) as! UIImageView!).image = nil
        URLImage().downloadImage(self.data[indexPath.row].image!) { (file, error) -> Void in
            if (error != nil) {
                return
            }
            var needShow : Bool! = false
            let indexPathies:NSArray? = tableView.indexPathsForVisibleRows
            if (indexPathies == nil) {return}
            for indexPathVisible in indexPathies! {
                if indexPathVisible.row == indexPath.row {
                    needShow = true
                    break
                }
            }
            if needShow == true {
                let dataFile : NSData? = NSData(contentsOfURL: file!)
                if dataFile == nil {return}
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    (cell.viewWithTag(3) as! UIImageView).image = UIImage(data: dataFile!)
                })
            }
        }

        (cell.viewWithTag(4) as! UILabel!).text = self.data[indexPath.row].author
        (cell.viewWithTag(5) as! UILabel!).text = String(self.data[indexPath.row].views!)
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 400
    }
    func refresh(refreshControl: UIRefreshControl?) {
        if (self.isLoading) {return}
        
        self.isLoading = true
        NewsModel.sharedInstance.getNews(useCache:self.loadCache, callback: {(result: Bool, data:[NewsItem])->Void in
            if(!self.loadCache) {
                self.isLoading = false
            }
            self.loadCache = false
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if (result) {
                    self.data = data
                    self.tableView.reloadData()
                }
                if (refreshControl != nil) {
                    refreshControl!.endRefreshing()
                }
            })
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "DetailView") {
            let cell : UITableViewCell! = sender as! UITableViewCell
            let indexPath : NSIndexPath? = self.tableView.indexPathForCell(cell)
            if (indexPath != nil) {
                (segue.destinationViewController as! ViewController).titleValue = self.data[indexPath!.row].title
                (segue.destinationViewController as! ViewController).urlValue = self.data[indexPath!.row].url
            }
        }
    }
}

