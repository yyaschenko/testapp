//
//  ViewController.swift
//  TestNewsApp
//
//  Created by Yurii on 2/17/16.
//  Copyright © 2016 Nostris. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var titleValue:String?
    var urlValue:String?
    @IBOutlet weak var webView:UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.titleValue;
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.webView.loadRequest(NSURLRequest(URL: NSURL(string: self.urlValue!)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringCacheData, timeoutInterval: 3000))
    }
}

